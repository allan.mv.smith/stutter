Game = require('./game.js');

Array.prototype.remove = function(from, to) {
	var rest = this.slice((to || from) + 1 || this.length);
	this.length = from < 0 ? this.length + from : from;
	return this.push.apply(this, rest);
}

function Table(tableID){
	this.id = tableID;
	this.players = [];
	this.playersID = [];
	this.gameObj = null;
}

Table.prototype.addPlayer = function(player) {
	var found = false;

	for(var i = 0; i < this.players.length; i++) {
		if(this.players[i].id == player.id) {
			found = true;
			break;
		}
	}
	if(!found) {
		this.players.push(player);
		return true;
	}
	return false;
}

Table.prototype.removePlayer = function(player) {
	var index = -1;
	for(var  i = 0; i < this.players.length; i++) {
		if(this.players[i].id === player.id) {
			index = i;
			break;
		}
	}
	if(index != -1) {
		this.players.remove(index);
	}
}

module.exports = Table;
