function Game() {
	this.deck = [1,2,3,4,5,6,7,8,9,10,11,12,13,1,2,3,4,5,6,7,8,9,10,11,12,13,1,2,3,4,5,6,7,8,9,10,11,12,13,1,2,3,4,5,6,7,8,9,10,11,12,13,1,2,3,4,5,6,7,8,9,10,11,12,13,1,2,3,4,5,6,7,8,9,10,11,12,13];
	this.deck = this.shuffle(this.deck);
	this.board = [];
	this.valids = [];
}

// Fisher Yates ShuffleShuffleShuffleShuffleShuffle
Game.prototype.shuffle = function(array){
	var m = array.length, t, i;

	// While there remain elements to shuffle…
	while (m) {

		// Pick a remaining element…
		i = Math.floor(Math.random() * m--);

		// And swap it with the current element.
		t = array[m];
		array[m] = array[i];
		array[i] = t;
	}

	return array;
}

Game.prototype.drawBoard = function(state) {
	if (typeof state === 'undefined' || state.length == 0) {
		this.board = this.resetBoard();
	} else {
		this.board = state;
	}
}

Game.prototype.resetBoard = function () {
	var newBoard = [];

	newBoard = this.drawCards(2);
	return newBoard;
}

Game.prototype.drawCards = function(drawSize) {
	cards = this.deck.splice(0,drawSize);
	return cards;
}

Game.prototype.createValids = function() {
	this.valids = [];

	for(i = 0; i < this.board.length; i++) {
		// the first two conditionals are for the Aces and the Ones
		if(this.board[i] == 13) {
			this.valids.push(1);
			this.valids.push(12);
		} else if(this.board[i] == 1) {
			this.valids.push(13);
			this.valids.push(2);
		} else { // else card is not an Ace or One
			this.valids.push(this.board[i]+1);
			this.valids.push(this.board[i]-1);
		}
	}
}

Game.prototype.fillHand = function(hand) {
	// Maximum size of hand
	var maxLength = 5;

	while(hand.length < maxLength) {
		hand.push(this.drawCards(1).pop());
	}
}

Game.prototype.isValid = function(card){
  return this.valids.indexOf(card) > -1 ? true:false;
}

Game.prototype.isValidInHand = function(hand){
	return	hand.some(this.isValid,this);

}

Game.prototype.isAvailableMove = function(hand1, hand2) {
	if(this.isValidInHand(hand1)  || this.isValidInHand(hand2))
		return true;
	else
		return false;
}

Game.prototype.isPlayInHand = function(hand, card) {
  return hand.indexOf(card) > -1 ? true: false;
}

module.exports = Game;
