function Player(playerID) {
	this.id = playerID;
	this.tableID = "";
	this.hand = [];
	this.score = 0;
};

Player.prototype.setTableID = function(tableID) {
	this.tableID = tableID;
};

Player.prototype.getTableID = function() {
	return this.tableID;
};

module.exports = Player;
