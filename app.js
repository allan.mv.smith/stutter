var Room = require('./room.js');
var Game = require('./game.js');
var Table = require('./table.js');
var Player = require('./player.js');

// app.js
var express = require('express'),
    app = express(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server);

server.listen(process.env.PORT || 3000);
io.set('log level', 1);
app.use('/',express.static(__dirname ));

app.get('/', function(req,res) {
  res.sendfile(__dirname + '/index.html');
})

var users = 0;
var room = new Room("Test Room");
// TODO:
// if deck is empty stop giving when NaN (might not be necessary)
// NOTE:
// there is an issue where if a card can be played on either pile
// it only plays it on the first (i'm not fixing this one though because
// it will require the player to choose the pile he wants to play the
// card on which is either click and drag or send the card and the pile
// intedned to play on from the client or have a subroutine popup when the
// card can be played on either pile.  either way it occurs rarely but
// frequently enough to warrant a change later should more extensive
// development be desired)


// takes an array and creates a unique random number
// there is a maximum of 100 set of IDS just for
// simplicity at this stage
function randomId(arr) {
  do {
    n = Math.floor(Math.random()*100+1);
  }
  while(arr.indexOf(n) !== -1);
  return n;
}

io.sockets.on('connection', function(socket) {
  var player = new Player(socket.id);
  room.addPlayer(player);

  // Initialize first table
  if(room.tables.length == 0) {
    var game = new Game();
    var table = new Table(randomId(room.tableIds));

    room.tableIds.push(table.id);
    table.gameObj = game;
    table.deck = table.gameObj.deck;
    room.tables.push(table);
    currentTable = room.tables[0];
  }

  // check if last table has a vacant spot
  // if not create a new table
  if (currentTable == 'new' || currentTable.players.length == 2) {
    var game = new Game();
    var table = new Table(randomId(room.tableIds));

    room.tableIds.push(table.id);
    table.gameObj = game;
    room.tables.push(table);
    currentTable = room.tables[room.tables.length - 1];
    currentTable.addPlayer(player);
    player.setTableID(currentTable.id);
    currentTable.gameObj.fillHand(player.hand);

    //player one is waiting at this point in a newly created table
    socket.emit("displayHand", {hand: player.hand});

  } else if (currentTable.players.length == 0 || currentTable.players.length == 1) {
    currentTable.addPlayer(player);
    player.setTableID(currentTable.id);
    // the second player joins (or in the case of the very
    // first player to join the server) and the board is created
    // now we must emit it to clients
    currentTable.gameObj.fillHand(player.hand);

    socket.emit("displayHand", {hand: player.hand});
  }


  // need to only create and  emit it when it is the
  // second player who has jioned need to emit to the
  // other player
  if(currentTable.players.length == 2) {
    do {
      currentTable.gameObj.drawBoard();
      currentTable.gameObj.createValids();
    } while(!currentTable.gameObj.isAvailableMove(currentTable.players[0].hand,currentTable.players[1].hand))

    // emit board to clients in table
    // using socket rooms is probably better as we can just
    // io.to('some room').emit('event')
    // socket.join('some room');
    for(var i = 0; i < currentTable.players.length; i++) {
      io.sockets.socket(currentTable.players[i].id).emit("boardUpdate",{board: currentTable.gameObj.board});
    }
  }

  socket.on("playcard", function(card) {
    var player = room.getPlayer(socket.id);
    var table = room.getTable(player.tableID);
    // make sure there are two players otherwise they are
    // trying to play with no board
    if (table.players.length == 2) {
      // determine opponent's socket.id
      for(var i = 0; i < table.players.length; i++) {
        if(!(table.players[i].id == player.id))
          var otherPlayer = table.players[i];
      }

      if (player.hand.indexOf(card) > -1) {
        if(table.gameObj.isValid(card)) {
          // card is valid
          // determine which pile to play card on
          var playedOn = table.gameObj.valids.indexOf(card);

          if (playedOn <= 1) {
            table.gameObj.drawBoard([card,table.gameObj.board[1]]);
          }else if (playedOn >= 2) {
            table.gameObj.drawBoard([table.gameObj.board[0],card]);
          }

          player.score++;
          socket.emit("scoreUpdate", {score: player.score, opponentScore: otherPlayer.score});
          io.sockets.socket(otherPlayer.id).emit("scoreUpdate", {score: otherPlayer.score, opponentScore: player.score});

          // check if player won else update their hand and valids
          if(player.score == 20) {
            socket.emit("displayMessage", {message: "You Win"});
            io.sockets.socket(otherPlayer.id).emit("displayMessage", {message: "You Lose"});
          } else {
            // need to remove card from hand and re-emit to client
            var cardPosition = player.hand.indexOf(card);
            player.hand.splice(cardPosition,1);
            table.gameObj.fillHand(player.hand);
            socket.emit("displayHand", {hand: player.hand});
            table.gameObj.createValids();
          }

        } else {
          console.log("invalid card from",socket.id);
        }
      } else {
        console.log("Card not in hand from",socket.id);
      }

  //    new board is made need to emit
      while(!table.gameObj.isAvailableMove(table.players[0].hand,table.players[1].hand)) {
        table.gameObj.drawBoard();
        table.gameObj.createValids();
      }

      // emit board to clients in table
      // using socket rooms is probably better as we can just
      // io.to('some room').emit('event')
      // socket.join('some room');
      for(var i = 0; i < table.players.length; i++) {
        io.sockets.socket(table.players[i].id).emit("boardUpdate",{board: table.gameObj.board});
      }
    }
  });

  socket.on("disconnect", function() {
    if(room.getPlayer(socket.id) != null) {
      var player = room.getPlayer(socket.id);
      var table = room.getTable(player.tableID);
      var playerIds = [];

      for (var x = 0; x < table.players.length; x++) {
         playerIds.push(table.players[x].id);
         io.sockets.socket(table.players[x].id).emit("displayMessage", {message: "They Left"});
      }

      for (var x = 0; x < playerIds.length; x++) {
        table.removePlayer(room.getPlayer(playerIds[x]));
        room.removePlayer(room.getPlayer(playerIds[x]));
      }
      // if the current table is the one that is being deleted
      if(currentTable.id === table.id) {
        currentTable = 'new';
      }
      // remove from room.tableIds
      for(var i = 0; i < room.tableIds.length; i++) {
        if(room.tableIds[i] == table.id) {
          room.tableIds.splice(i,1);
        }
      }

      room.removeTable(table);
    }
  });

});
